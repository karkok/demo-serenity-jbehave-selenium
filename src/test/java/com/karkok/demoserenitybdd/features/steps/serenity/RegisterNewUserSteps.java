package com.karkok.demoserenitybdd.features.steps.serenity;

import com.karkok.demoserenitybdd.features.pageObjects.RegistrationPageObject;

import net.thucydides.core.annotations.Step;
import net.thucydides.core.pages.Pages;
import net.thucydides.core.steps.ScenarioSteps;

public class RegisterNewUserSteps extends ScenarioSteps {

	private static final long serialVersionUID = -6509811886419741639L;

	public RegistrationPageObject registrationPage;
		
	public RegisterNewUserSteps(Pages pages) {
		super(pages);
	}
	
	@Step
	public void openRegistrationForm() {
		registrationPage.open();
	}
	
	@Step
	public void typeLoginPasswordAndSubmit(String login, String password) {
		registrationPage.typeLogin(login);
		registrationPage.typePassword(password);
	}
	
	@Step
	public boolean checkErrorMsg(String errorMsg) {
		return registrationPage.containsError(errorMsg);
	}
	
}
