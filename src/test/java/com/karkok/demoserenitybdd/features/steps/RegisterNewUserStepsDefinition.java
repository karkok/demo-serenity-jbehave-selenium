package com.karkok.demoserenitybdd.features.steps;

import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Named;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;

import com.karkok.demoserenitybdd.features.steps.serenity.RegisterNewUserSteps;

import net.thucydides.core.annotations.Steps;

public class RegisterNewUserStepsDefinition {
	
	@Steps
	private RegisterNewUserSteps steps;
	
	@Given("registration form DEMO")
	public void registrationForm() {
		steps.openRegistrationForm();
	}
	
	@When("I want to register non existing <login> and <password> DEMO")
	public void registerWithLoginAndPassword(@Named("login") String login, @Named("password") String password) {
		steps.typeLoginPasswordAndSubmit(login, password);
	}
	
	@Then("I should be told that my password is less than 8 characters long")
	public void passwordIsLessThan8Characters() {
		steps.checkErrorMsg("Password must be at least 8 characters in length.");
	}
	
}
