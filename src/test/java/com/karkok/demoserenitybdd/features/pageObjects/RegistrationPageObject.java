package com.karkok.demoserenitybdd.features.pageObjects;

import org.openqa.selenium.WebDriver;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("http://localhost:8080")
public class RegistrationPageObject extends PageObject {
	
	@FindBy(id = "login")
	private WebElementFacade loginInput;
	
	@FindBy(id = "password")
	private WebElementFacade passwordInput;
	
	@FindBy(id = "passwordError")
	private WebElementFacade passwordErrorBox;

	public RegistrationPageObject(WebDriver driver) {
		super(driver);
	}
	
	public void typeLogin(String login) {
		loginInput.type(login);
	}
	
	public void typePassword(String password) {
		passwordInput.typeAndEnter(password);
	}
	
	public boolean containsError(String errorMsg) {
		return passwordErrorBox.containsText(errorMsg);
	}

}
