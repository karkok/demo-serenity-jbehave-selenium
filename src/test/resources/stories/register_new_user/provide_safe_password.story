Narrative:
In order to keep users passwords safe
As an administrator
I would like to require on end users to provide safe password during registration.

Scenario: A scenario to verify short passwords
Given registration form DEMO
When I want to register non existing <login> and <password> DEMO
Then I should be told that my password is less than 8 characters long
Examples:
| login | password  |
| user  | 1234      |


Scenario: A scenario to verify whitespaces
Given registration form 
When I want to register non existing <login> and <password>
Then I should be told that whitespaces are not allowed
Examples:
| login | password     |
| user  | 12 34        |
| user  | ab c.        |
| user  | ;254ggfee hf |


Scenario: A scenario to verify numeric characters
Given registration form 
When I want to register non existing <login> and <password>
Then I should be told that password must contains at least one numeric character
Examples:
| login | password     |
| user  | abcdefg.gd.  |
| user  | onetSronet@  |
| user  | onlyTheBest# |


Scenario: A scenario to verify non-alphanumeric characters
Given registration form
When I want to register non existing <login> and <password>
Then I should be told that password must contains at least one non-alphanumeric character
Examples:
| login | password     |
| user  | abcdefggd    |
| user  | onetSronet21 |
| user  | onlyThe4Best |


Scenario: A scenario to verify lowercase character
Given registration form
When I want to register non existing <login> and <password>
Then I should be told that password must contains at least one lowercase character
Examples: 
| login | password     |
| user  | ABCDEFG1.F4  |
| user  | OOERD#$@17   |
| user  | ONLY232@!#!  |


Scenario: A scenario to verify uppercase scenario
Given registration form
When I want to register non existing <login> and <password>
Then I should be told that password must contains at least one uppercase character
Examples:
| login | password     |
| user  | abcdefg.gd.  |
| user  | onetsronet@  |
| user  | onlythebest# |

